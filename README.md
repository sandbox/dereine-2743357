# Field Linker

This module provides a way to wrap any existing field out there with a link to
its original entity.

## Install the module

Install the module as every other Drupal module.

## Usage of the module

Go to the manage display tab and choose the 'field linker' formatter. In there
you can select the formatter of the underlying field.
