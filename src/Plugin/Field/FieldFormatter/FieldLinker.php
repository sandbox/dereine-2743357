<?php

namespace Drupal\field_linker\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the field linker formatter.
 *
 * @FieldFormatter(
 *   id = "field_linker",
 *   module = "field_linker",
 *   label = @Translation("Field linker"),
 *   field_types = {
 *   },
 * )
 *
 * 
 */
class FieldLinker extends FieldWrapperBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $field_output = $this->getFieldOutput($items, $langcode);

    $elements = [];
    foreach (Element::children($field_output) as $key) {
      $elements[$key] = [
        '#type' => 'link',
        '#url' => $items->getEntity()->toUrl(),
        '#title' => $field_output[$key],
      ];
    }
    return $elements;
  }

}
