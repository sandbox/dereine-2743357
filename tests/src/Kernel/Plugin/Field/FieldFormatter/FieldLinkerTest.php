<?php

namespace Drupal\Tests\field_linker\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\field_linker\Plugin\Field\FieldFormatter\FieldLinker
 * @group field_linker
 */
class FieldLinkerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['entity_test', 'field_linker', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
  }

  public function testFieldLinkerFormatter() {
    $entity = EntityTest::create([
      'name' => 'test name',
    ]);
    $entity->save();

    $build = $entity->name->view([
      'type' => 'field_linker',
      'settings' => [
        'type' => 'string',
      ],
    ]);

    $output = \Drupal::service('renderer')->renderRoot($build);
    $this->setRawContent($output);

    $href = (string) $this->cssSelect('a')[0]->attributes()['href'];
    $this->assertEquals($href, $entity->toUrl()->toString(TRUE)->getGeneratedUrl());
    $this->assertText('test name');
  }

}
